export SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
export SCRIPT_DIR="."

echo "Usage:  > source alias.sh"
echo "New macros."
echo "   run:   rebuild blog and show it in browser"
alias run='fades -r $SCRIPT_DIR/requirements.txt -x nikola build && fades -r $SCRIPT_DIR/requirements.txt -x nikola serve -b'

echo " build:   rebuild blog"
alias build='fades -r $SCRIPT_DIR/requirements.txt -x nikola build'

echo " serve:   show blog in browser"
alias serve='fades -r $SCRIPT_DIR/requirements.txt -x nikola serve -b'

echo "nikola:   give other nikola directive (use 'nikola help' to see them)"
alias nikola='fades -r $SCRIPT_DIR/requirements.txt -x nikola'

echo "  kill:   kill nikola serve process"
alias kill='pkill -9 nikola'

echo "  cdbl:   change to blog directory"
alias cdbl='cd "$SCRIPT_DIR"'

