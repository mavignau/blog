.. title: Análisis encuesta asistentes
.. date: 2020-02-01 09:22:34 UTC+03:00
.. tags: análisis, datascience, encuesta
.. category: blog
.. link:
.. description:
.. type: text

Decidí que necesitaba algo más de información respecto del perfil y las preferencias del público
que asistió a los eventos que coorganicé en el año pasado.

Quiero así también conocer qué tal estamos yendo, la edad y el género promedio de las personas que
asistieron. Así que envié emails a todos los emails registrados, enviándoles una salutación de fin de
año y también mandándoles el link para completar la encuesta. También puse en todos los grupos en redes
sociales que surgieron de las reuniones, Telegram, Whatsapp, Facebook y por Twitter.

El resultado final pueden verlo acá:

`resultado final <https://mavignau.gitlab.io/blog/stories/analisis-de-la-encuesta/index.src.html>`_.


Proceso de análisis
=====================

Una vez obtenidos los datos, el desafío fué bajar la información. Encontré que podía abrir planilla
de cálculos con los resultados y posteriormente exportar a CSV (*Comma Separated Values*).

Conectando con la API de Google
---------------------------------

Pero luego me sugirieron conectar directamente con la API de Google, y así fué que cree un proyecto en
Google usando `la consola Google <https://console.cloud.google.com/>`_.

Fuí al dashboard del proyecto, y de ahí seleccioné APIs, y navegué a la `biblioteca de APIs
<https://console.cloud.google.com/apis/library>`_.

Habilité Google Drive API, luego fuí a crear credenciales. Allí elegí que la llamaría a la API desde
otra interfaz de usuario (CLI, Windows) y que necesitaría usar datos de usuario.

Luego cree una cuenta de servicio llamada encuesta, y bajé las credenciales en formato json. Las copié al
subdirectorio en el que se encontraba mi notebook jupyter.

Ahí fuí a la hoja de cálculo GCalc, y la compartí con el usuario de la cuenta de servicio, que tiene un
email que termina en ``encuesta@.....iam.gserviceaccount.com``.

Creando el notebook de jupyter para análisis de los datos
-----------------------------------------------------------

Para crear la notebook jupyter, cree un nuevo environment y los añadí como kernel a jupyter.
Para esto cree el virtualenv

>>> conda create -n myenv

Activé el environment creado

>>>  conda activate myenv

Luego instalé el paquete que provee el kernel IPython para Jupyter

>>> pip install --user ipykernel

Y finalmente lo activé

>>> python -m ipykernel install --user --name=myenv

Cree un nuevo notebook con el environment, y procedi a crear una celda especial para
instalar los siguientes paquetes:

>>> import sys
>>> !{sys.executable} -m pip install pandas, numpy, matplotlib, gspread, oauth2client

Presentación y publicación de los resultados
------------------------------------------------

Una vez instalado todo, utilizando las credenciales de google anteriormente bajadas, agregué
el código para bajar la información directamente de la planilla y para realizar el análisis.

Una vez que completé el análisis, y realicé los gráficos correspondientes, procedí a exportar
en formato HTML el resultado. El problema fué que cuando mostré los mismos, me dijeron que
el hecho de que el código de programación python se entremezcle con los resultados resultaba
confuso.

Para evitar esto, al html exportado le agregué el siguiente código:

::

    <script>
    function myFunction() {
        var x = document.getElementsByClassName("highlight");
        var i;
        for (i = 0; i < x.length; i++) {
          if (x[i].style.display === "none") {
            x[i].style.display = "block";
          } else {
            x[i].style.display = "none";
          }
        }
    }
    </script>

Después, dentro del bloque de la descripción inicial agregué un botón:

::

    <button onclick="myFunction()">Mostrar Ocultar código fuente</button>

Luego del tag body, en el evento onload, también llamé a la función para que al leer
la página también lo oculte:

::

    <body onload="myFunction()">


Finalmente, lo agrego a mi blog como una página estática, en el subdirectorio stories, con formato html.
Para que el generador de páginas estáticas Nikola lo agregue, precedí con

::

    <!--
    .. title: Análisis de la encuesta
    -->

El resultado final pueden verlo acá:

`resultado final <https://mavignau.gitlab.io/blog/stories/analisis-de-la-encuesta/index.src.html>`






