.. title: Crónica del PyDay
.. date: 2022-12-03 9:22:34 UTC-03:00
.. tags: evento, relato
.. category: blog
.. link:
.. description:
.. type: text


Hace tiempo
^^^^^^^^^^^

Todo empezó hace unos cuantos meses. Dijimos con mi amigo Pelín
“¡hagamos un pyday!”


Un par de meses pasaron y todavía no se hacía nada, así que le dije
“pongamos una fecha”.

     Al principio de todo se puede tener preparadas algunas cosas: por
    ejemplo, la descripción del evento que deseamos y algunas plantillas
    para la parte gráfica. Plantilla de e-mail.

Dos meses antes
^^^^^^^^^^^^^^^

Él propuso su empresa como lugar, porque allí tendríamos
probablemente un catering y una pantalla gigante para proyectar.
Claro que, como mucho, podríamos tener unos 25 asistentes.


La fecha fue una difícil decisión: primero podía ser un 6 de
noviembre, pero después esto resultó muy pronto y quisimos mandarla
al sábado 19. Esto le dije que era mejor que no, porque en ese mismo
día los chicos de Catamarca estaban organizando el suyo. Así que
quedó para el día 26 de noviembre de 2022.

    La decisión de la fecha es algo que también debería generar un más
   amplio pedido de opinión. Además, hacerlo con un mínimo de tres
   meses.


Propuse dos opciones de imagen gráfica y pedí opinión a la comisión
directiva de pyar, y finalmente quedó la primer idea que tuve.

50 días antes
^^^^^^^^^^^^^

Hacia el 10 de octubre, ingresé a la instancia de EventoL de Python
Argentina, con mi usuario cree el evento, subí la imagen que habíamos
armado, ingresé la fecha de realización, cree una sala única del
evento, puse el lugar de realización, la empresa de Pelín.


    En este momento tendríamos que haber redactado también una
   descripción textual del evento, para poder ponerla en la instancia de
   eventoL

Y luego publiqué en mis redes sociales de pythonistas el llamado a
propuestas con el link generado en eventoL para que la gente se
anote.


    Aquí era el momento idóneo para buscar sponsors, tanto de python
   argentina, como en empresas locales o gobierno. Así, con cada
   respuesta positiva se puede anunciar algo en redes y medios para ir
   generando expectativa en la personas. Y aquí debí empezar a pedir
   colaboradores: alguien que quiera ir al evento y encargarse de video,
   grabación y fotografía.


40 días antes
^^^^^^^^^^^^^

Cree una charla en mi canal de youtube, que esperaba sirviera para dar
impulso a la gente que quisiera animarse a ser speaker el día 19 de
octubre. Traté de darle difusión y luego me puse a esperar.

1 mes antes
^^^^^^^^^^^

Nos avisaron que había otro lugar disponible para nuestro evento.
Después de un poco de indecisión, nos pareció más amplio que el lugar
original. Me dieron el contacto de la persona a cargo de administrarlo.
Así que me puse en contacto con ella


25 días antes
^^^^^^^^^^^^^

Sergio Lapertosa nos comentó que una persona, Samuel Noya, que podría
proponerse y le había pasado el contacto para hacerlo. Nunca lo hizo.

     Un problema aparte es la reticencia de algunos speakers de formalizar
    en EventoL. El pedido de propuesta es abierto, pero igual parecen dudar
    de describir su propuesta en el sitio. Quizá brindar un sistema de chat
    inmediato para hablar con los organizadores simplificaría las cosas.

Nos decidimos por la Usina, que nos pareció más amplio que el lugar
original. Me dieron el contacto de la persona a cargo de administrarlo.
Así que me puse en contacto con ella. Al principio, creíamos que nos
proveerían de un pequeño coffee break.


24 días antes
^^^^^^^^^^^^^

Llegó la primera propuesta, e inmediatamente me puse a hacer el cartel
gráfico para anunciarla por todas partes.

     Con la guía del coordinador o “redactor jefe” del evento, y del
    diseñador gráfico, se tiene que hacer marketing en las principales redes
    sociales. El trabajo de manejo de comunidades implica que alguien debe
    tener, en este caso, Instagram, Twitter, Facebook, Linkedin, y estar
    incluido en las comunidades de Python locales. Y cualquier otra red
    social donde la gente se agolpe.

    El administrador de comunidades es un rol central en todo momento.

Lamentablemente, en ese momento no manejaba bien EventoL y no me resultó
nada amigable encontrar como avisar a los que propusieron que su
propuesta estaba aceptada.

     A los speakers hay que darles feedback siempre. Son el eje del
    espectáculo. El hecho de dar por supuesto que la gente que vendría de
    lejos estaba en las mismas redes y asumiría que al estar difundiendo su
    propuesta es que nos gustó implicó que después se bajaran. EventoL no
    permite a la gente ingresar más contacto que un email, y, encima, no lo
    pone en un lugar que me resultara intuitivo a mí.


8 días antes
^^^^^^^^^^^^

Ante la falta de propuestas, el día 18 de noviembre anoté una charla
mía, que prefería ensayarla antes de proponerla en otros foros
internacionales.

1 semana antes
^^^^^^^^^^^^^^

Al día siguiente empecé a desesperar: faltaban propuestas, faltando
apenas una semana, no podía confirmarles a los chicos de la propuesta
sobre testing y no aparecía más gente. Pedía ayuda en el grupo de
organización. Dos de las chicas que querían aportar. Una de ellas, Zoe,
al fijarse en su charla, decidió que no sería apta para una audiencia
que recién empezaba con la programación. Valeria, la otra, me empezó a
hablar, y nos pusimos a elaborar su propuesta.

También formalizó Matías, que inscribió su propuesta Introducción a
Python el día 22 de noviembre. Ya contaba con su colaboración desde
hacía un mes, pero lamentablemente no tenía una descripción formal y no
podía agregarla al cronograma.

Me dijo que en la Usina nos ayudarían con la difusión. Esto se cumplió
de forma muy tibia.

Para conocer bien de cerca el lugar donde haríamos el evento, fui a
otro evento informático la semana previa. Allí pude ver que eran muy
lindas las instalaciones, pero me dijeron que no proveían ninguna
ayuda con la comida y que tampoco tenían pantallas para que usáramos:
deberíamos traer un proyector propio. Y alguna tela o algo así para
poder ver la imagen, ya que no hay ninguna pared blanca apropiada.

El proyector sencillamente traje el mío, que es apto para eventos
diurnos porque tiene bastante potencia de imagen. Sin embargo, con la
pantalla de proyección estuvo más difícil. Por suerte, un amigo de Pelín
tenía la pantalla para la proyección. Sino, hubiéramos debido improvisar
con alguna tela blanca, quizá.


5 días antes
^^^^^^^^^^^^

Después de ver en youtube una charla donde decía que uno de los
principales tips que daban era asegurar que la gente tuviera suficiente
para comer y beber. En el evento que había ido la semana anterior al mío
me había gustado mucho el servicio, así que quedé con el contacto y le
solicité un coffee break. Estimé unas treinta personas asistiendo,
aunque en ése día martes apenas teníamos 8 inscriptos. Por suerte, la
encargada del catering quería mantenerme como cliente y de inmediato me
dijo que sí, que podría hacerlo.

Dos días antes del evento
^^^^^^^^^^^^^^^^^^^^^^^^^

Esto trajo algo que no había considerado: quedé adeudando. El coffee
break podía donarlo, pero igual me pareció pertinente pedir ayuda
económica. Así que hablé a Facundo Batista y el me pasó el modelo que
habían usado otros grupos para solicitar el dinero. Una vez que llené el
modelo con los datos de mi evento, el mandó a los contactos que tenía y
gracias a ello, parte del costo será eventualmente devuelto.

Armé el cronograma definitivo y empecé a difundirlo y solicitar que
difundan por redes sociales. Lamentablemente, no pude usar algunos
medios, como email o pegar carteles por la falta de personas dispuestas
a dar charlas.

36 horas antes
^^^^^^^^^^^^^^

Lamentablemente, todavía no sabía como asegurar la presencia de los
speakers que venían de lejos, ni ellos habían hecho esfuerzo alguno para
conectar. Me guiaron a la forma de ver en eventol el email que habian
dejado y allí me encuentro con que era muy tarde: ellos no habían
preparado su viaje y no vendrían.

Zoe S., mientras tanto, me aseguró que podría imprimir ése día viernes
el cronograma y los papeles con los códigos qr para la autoinscripción.


24 horas antes
^^^^^^^^^^^^^^

El día anterior al evento llegó Matías de Gualeguay, y lo recibí en mi
casa. Quedé arreglando las cuestiones últimas de agradecimiento a
nuestro único sponsor, ajustando los carteles que se imprimirían y
revisando las diapositivas de mi propia charla.

El día del evento
^^^^^^^^^^^^^^^^^

Al día siguiente me levanté muy temprano. Ensayé mi charla, depuré
algunos defectos y lo levanté a Matías, que estaba en mi departamento.
Armamos los bolsos con proyector, capturadora de video, cables,
computadoras y nos fuimos al lugar del evento, a 900 mts de ahí.

Cuando llegamos, abro el Whatsapp y Valeria, la oradora que se había
agregado para cerrar nuestro evento, no podía venir, se había
descompuesto a la noche.

De todas formas, pusimos la pantalla blanca con Pelín, movimos sillas y
mesas para adaptarlos a nuestro setup, calibramos el proyector para
máximo brillo en la sala tan iluminada, puse la video cámara a filmar.

A las 9:30, dí apertura al evento, comenté sobre el PyCamp en
Corrientes, sobre Python Argentina, e invité a Matías a empezar a
hablar.

Mientras tanto, con Pelín empezamos a ver qué hacer con la falta de dos
speakers.

Matías, que sabía del inconveniente, alargó un poco su charla. Mi
charla, que sería la tercera, la pusimos segunda. Luego del coffee
break, que fue un momento donde los asistentes pudimos ponernos en
contacto y disfrutar realmente del evento, llegó Pelín que se había ido
a buscar algunos elementos de su casa.

La charla de él fue todo un éxito, y luego Zoe dió una speed lighting de
introducción a GIT. Allí preparamos varias remeras donadas por Python
Argentina y stickers traídos de PyCon USA para sortear y repartir entre
los asistentes.

Después del sorteo, hablamos un poco varios de los programadores que ya
trabajabamos con Python sobre nuestras experiencias y consejos para los
que quieren empezar.

Después del evento
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Y hoy debo ingresar mi proveedora, mi sponsor y mis costos en el sistema
de registración de Python Argentina.

Aún falta editar los vídeos, y subirlos a Youtube.

