.. title: Cómo acelerar por harware operaciones con videos
.. date: 2020-04-27 23:22:34 UTC+03:00
.. tags: video, acelerar, linux
.. category: blog
.. link:
.. description:
.. type: text

Cómo acelerar por hardware operaciones con video.
=========================================================

Hola! Tengo un hw bastante modesto (i5, 8G RAM), Linux Mint y hago muy cada tanto algo de edición.
Así que estuve mirando cómo hacer para ver cómo era mi tarjeta gráfica, para ver si había algún recurso de aceleración que no había aprovechado. Encontré que debía hacer
>> lspci | grep VGA
>> sudo lshw -C video
ahí, supe que tenía una plaqueta gráfica intel HD Graphics 5500

OBS Studio
----------
En los ajustes avanzados de salida, apartado video, descubría que tenía la posibilida de comprimir usando hardware, pero cuando lo traté de habilitar, me informé de que faltaba habilitar la aceleración 
Ahí me informé que existe una biblioteca de uso de mi tarjeta gráfica llamada vaapi, así que busqué
``$ sudo apt-cache search vaapi``
ahí me encontré con dos paquetes que pocedí a instalar
``$ sudo apt-get i965-va-drivera
$ sudo apt-get i965-va-driver-shaders``
y entonces empezó mi obs studio a funcionar mucho mejor

Verificando el uso
------------------
Para poder ver el nivel de exigencia de mi tarjeta, instalé
``$ sudo apt install intel-gpu-tools``
Así al hacer
``$ sudo intel_gpu_top``
podría ver el nivel de exigencia

FFMpeg: transcodificando
------------------------
Después, encontré que ffmpeg también nos permite utilizar la gpu para transcodificar.
``$ ffmpeg -vaapi_device /dev/dri/renderD128 -i <inputfile> -vf 'format=nv12,hwupload' -c:v h264_vaapi <outfile>.mkv``

KDEnlive: Edición de videos
---------------------------
Finalmente, me encontré intentando lograr la aceleración de videos. Buscando, encontré que podría ser posible, pero primero es necesaria la biblioteca movit y rtaudio. 
``$ sudo apt-get install librtaudio6``
``$ sudo apt install libmovit8``

Encontré algún artículo [1]_, pero aparentemente sólo recompilando la librería MLT se podría hacer.

.. [#] `https://forum.manjaro.org/t/kdenlive-intel-gpu-acceleration-rendering-h264-video-boost-encoding/88829 <https://forum.manjaro.org/t/kdenlive-intel-gpu-acceleration-rendering-h264-video-boost-encoding/88829>`_


