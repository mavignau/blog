.. title: Bailo con tu sombra: patch, mock y stub.
.. date: 2022-02-07 23:22:34 UTC+03:00
.. tags: charlas historial refencias
.. category: stories
.. link:
.. description:
.. type: text


Abstract
============
Explica el uso de objetos falsificados o mocks para testing.

Descripción
===============
Stubs y mocks son técnicas diversas de testing, que pueden ser implementadas usando la función mock de la biblioteca estándar. 
Patch permite inyectar estos mocks como sustitutos en lugares estratégicos del código mediante multiples técnicas. 
Su uso suele llevar a confusión al iniciarse con los testeos unitarios en Python

Contenido
=========
- Porqué usar simuladores
- Cómo usar patch.
- Simuladores: stub y mock.
- Librerías para desarrollo web

Recursos
=============
🎞 `Video PyConAr 2011 <https://youtu.be/cgY7ZP0W9g4>`_

✍ `Propuesta PyConAr_2021 <https://eventos.python.org.ar/events/pyconar2021/activity/491/>`_

💾 `Diapositivas: </Mocks_es.pdf>`_





