.. title: Triunfar con Python: Vivencias y reflexiones de una programadora.
.. date: 2022-02-07 23:22:34 UTC+03:00
.. tags: charlas historial refencias
.. category: stories
.. link:
.. description:
.. type: text


Abstract
============
Repaso de mi historia con la mi profesión como informática y los desafíos que tuve que afrontar.

Descripción
===============
Mi interés por el software libre, las redes y las comunidades vienen desde los '80, cuando participaba en FidoNET, copiaba código de 
revistas como Commodore World y desarrollaba en casa. Se renueva con Python, su gran atractivo y la vibrante comunidad argentina 
formada con el lenguaje . Luego, paso a paso, desde la participación casi turística en los primeros eventos, a la cooperación con 
PR chicos en proyectos muy seleccionados, y dando charlas primero en mi ciudad, luego en otras ciudades y en el exterior, 
primero en español y luego en inglés, fui cumpliendo las metas propuestas. 
Finalmente, este año, logré conseguir trabajo como dev remoto usando la tecnología que elegí.

Recursos
=============
🎞 :  `PyConAr 2021 <https://youtu.be/XVKH45CGVvY>`_

✍ :  `PyConAr_2021 <https://eventos.python.org.ar/events/pyconar2021/activity/489/>`_


