.. title: Terminaltopia
.. date: 2022-02-07 23:22:34 UTC+03:00
.. tags: charlas historial refencias
.. category: stories
.. link:
.. description:
.. type: text

Abstract
============
¿Porqué usamos tanto el ratón, si podemos tener mayor velocidad usando el teclado? Voy a mostrar 
como existen poderosas y maravillosas herramientas para una consola super poderosa, ágil y elegante en Linux.

Descripción
===============
Presento un pequeño set de herramientas que facilitan el trabajo como desarrollador.
Centradas en el uso de la consola como herramienta fundamental.

Contenido
=============
- Motivos para mejorar la consola.
- Demostración de herramientas
  1. NeoVim
  2. Kitty
  3. LazyGIT
  4. Fish
  5. Ranger
  6. NerdFonts
  7. Linker.py

Recursos
=====================================

- 💾: `Diapositivas Terminal Utopía </TerminalUtopia.pdf>`_

NeoVim
^^^^^^
- `NeoVim <https://neovim.io/>`_
- `Config Python <https://vim.fisadev.com/>`_

Kitty
^^^^^^
- `Kitty <https://sw.kovidgoyal.net/kitty/>`_

Fish Shell
^^^^^^^^^^^^^^^^^^
- `Playground <https://rootnroll.com/d/fish^shell/>`_
- `Fisher <https://github.com/jorgebucaran/fisher>`_
- `Ssh agent <https://github.com/danhper/fish^ssh^agent>`_
- `Z <https://github.com/jethrokuan/z>`_
- `fzf <https://github.com/PatrickF1/fzf.fish>`_

NerdFonts
^^^^^^^^^^^^^^^^^^
- `Readme del repo <https://github.com/ryanoasis/nerd^fonts/blob/master/readme_es.md>`_
- `Site <https://www.nerdfonts.com/>`_

LazyGit
^^^^^^^^^^^^^^^^^^
- `lazygit <https://github.com/jesseduffield/lazygit>`_
- `deltagit <https://github.com/dandavison/delta>`_

Más software interesante
~~~~~~~~~~~~~~~~~~~~~~~~

Tmux
^^^^^^
- `Tmux <https://www.hostinger.es/tutoriales/usar^tmux^cheat^sheet>`_
- `Tmuxp <https://tmuxp.git-pull.com/index.html>`_

More
^^^^^^^^^^^^^^^^^^
- `Ls Deluxe <https://github.com/Peltoche/lsd>`_
- `Bat: Cat with Wings <https://github.com/sharkdp/bat>`_
- `LookAtMe <https://pypi.org/project/lookatme/>`_
- `LazyDocker <https://github.com/jesseduffield/lazydocker>`_


