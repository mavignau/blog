.. title: Introducción a Python con Jupyter Lab
.. date: 2022-02-07 23:22:34 UTC+03:00
.. tags: charlas historial refencias
.. category: stories
.. link:
.. description:
.. type: text


Abstract
============
Python tiene muchas herramientas poderosas para científicos e ingenieros. Esta charla introduce algunos conceptos sobre las capacidades matemáticas del lenguage usando Jupyter como entorno de desarrollo. 

Descripción
===============
Presento herramientas informáticas apropiadas para el uso en ingenierías y ciencias.
La charla está orientada a matemáticos, científicos e ingenieros sin conocimientos de programación.

Contenido
=============
- Razones para usar python
- Razones para usar Jupyter Lab
- Demostración de tipos de números soportados, funciones matemáticas, estadísticas. listas, cadenas y diccionarios.
- Enumeración y descripción de bibliotecas significativas
- Libros y comunidades recomendadas

Recursos
=============

Diapositivas y notebook
~~~~~~~~~~~~~~~~~~~~~~~

- 💾: `Introducción a Python con Jupyter </IntroduccionPythonConJupyter.pdf>`_
- 💾: `Introducción a Python con Jupyter: Demo Notebook </IntroduccionPythonConJupyterNB.ipynb>`_
- 💾: `Introducción a Python con Jupyter: Demo Notebook PDF </IntroduccionPythonConJupyterNB.pdf>`_

Bibliotecas mencionadas
~~~~~~~~~~~~~~~~~~~~~~~

- `Numpy <https://numpy.org>`_
- `SciPy <https://scipy.org>`_
- `Pandas <https://pandas.pydata.org>`_
- `Bokeh <https://bokeh.org>`_
- `SymPy <https://www.sympy.org>`_
- `ForAllPeople <https://github.com/connorferster/forallpeople>`_
- `PyNite <https://github.com/JWock82/PyNite>`_
- `GroundHog <https://groundhog.readthedocs.io/en/master/>`_

Libros mencionados
~~~~~~~~~~~~~~~~~~

- `Python en Ámbitos Científicos <https://pyciencia.taniquetil.com.ar/>`_
- `Curso de Python para ciencias e ingenierías <https://github.com/mgaitan/curso-python-cientifico>`_

Comunidades 
~~~~~~~~~~~~


Python Argentina
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
- `Web PyAr <https://www.python.org.ar>`_
- `Telegram PyAr <https://t.me/pythonargentina>`_
- `YouTube PyAr <https://www.youtube.com/channel/UCjYLIv07fw21w0uIAtUMnNA>`_

Python Científico Argentina
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
- `Telegram SciPy <https://t.me/scipyargentina>`_
- `SciPy Latinoamérica <https://www.scipy.lat/es/>`_
- `Python Científico Latino América 2022 <https://pythoncientifico.ar/>`_

Python NEA Argentina
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
- `Telegram PythonNEA <https://t.me/pydaynea>`_

Python en Español
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
- `Telegram Python Español <https://t.me/PythonEsp>`_
- `YouTube Python Español <https://www.youtube.com/channel/UClhDhiyKpVafTWfj4FYad1Q>`_
- `Hablemos Python <https://hablemospython.dev/>`_

