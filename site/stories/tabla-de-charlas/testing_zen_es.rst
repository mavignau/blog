.. title: The Zen of Python testing: beyond the debugger
.. date: 2024-12-07 23:22:34 UTC+03:00
.. tags: charlas testing
.. category: stories
.. link:
.. description:
.. type: text


Abstract
========
Sumérjase en los aspectos esenciales de las pruebas y la automatización para mejorar la calidad y la entrega del software. 
Aprenda por qué son importantes las pruebas, qué automatizar, tipos: unitarias, funcionales, de integración, de rendimiento y de usabilidad, 
filosofías TDD/BDD, mejores prácticas y herramientas como pytest. 
Obtenga conocimientos prácticos para crear software fiable y mantenible con mayor rapidez.


Description
===========
Liberar el poder de las pruebas automatizadas: Guía para principiantes para escribir mejor código».

Únase a esta charla especial para iniciar su viaje en el mundo de las pruebas automatizadas. Diseñado para desarrolladores de todos los niveles de experiencia, esta sesión se sumerge en los fundamentos de las pruebas y por qué la automatización es una parte esencial de la creación de software robusto, mantenible y escalable.

Empezaremos por lo básico, desglosando los conceptos básicos de las pruebas y explorando las razones cruciales para automatizar las pruebas. A partir de ahí, le guiaremos a través de los diferentes tipos de pruebas, incluidas las unitarias, funcionales, de integración, de rendimiento y de usabilidad, mostrando cómo cada una de ellas desempeña un papel clave a la hora de garantizar la calidad del software.

Pero las pruebas no son sólo una cuestión de herramientas, sino también de estrategia. Comprenderá mejor las filosofías de pruebas más populares, como el desarrollo basado en pruebas (TDD) y el desarrollo basado en el comportamiento (BDD), y aprenderá cómo estos enfoques pueden aportar claridad y enfoque al código. En el camino, discutiremos las mejores prácticas y conceptos prácticos, como mock y patch, que hacen que las pruebas sean eficientes y manejables.

Por último, presentaremos potentes bibliotecas como unittest, pytest y pytest_bdd, herramientas que puede empezar a utilizar inmediatamente para tomar el control de su proceso de pruebas.


Esquema
=======
Esta es una charla especial para iniciarse en el uso de pruebas automatizadas, 

En el formato de charla corta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- los conceptos básicos de las pruebas 

- razones para automatizar las pruebas, 

- tipos de pruebas: unitarias, funcionales, de integración, de rendimiento, de usabilidad. 

- filosofías de testing como test driven design y behavior driven design.

- principales bibliotecas para pruebas: unittest, pytest, pytest_bdd

- conceptos de mock, patch

- buenas prácticas en pruebas

This is a special talk to initiate the use of automated tests, 

It includes 

- the basic concepts of testing, 

- reasons to automate tests, 

- types of testing: unit, functional, integration, performance, usability. 

- testing philosophies such as test driven design and behavior driven design.

- main libraries for testing: unittest, pytest, pytest_bdd

- concepts of mock, patch

- best practices in testing

En el formato de taller (90 min)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Añadir una profundización de las características más usadas de pytest, incluyendo parámetros, fixtures.

Añadir ejemplos de BDD puestos en práctica con un ejemplo práctico completo. 

Discutir técnicas básicas de mocking y patching.


Recursos
=============
✍ : `Presentación PyLadies 2024 <https://pretalx.com/pyladiescon-2024/talk/HKYPSL/>`_

💾: `Slides version taller 90 min </ZenTesting_taller.pdf>`_

💾: `Slides version charla 30 min </ZenTesting_charla.pdf>`_

