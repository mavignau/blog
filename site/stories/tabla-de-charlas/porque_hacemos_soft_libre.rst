.. title: Porque hacemos Soft Libre
.. date: 2022-02-07 23:22:34 UTC+03:00
.. tags: charlas historial refencias
.. category: stories
.. link:
.. description:
.. type: text


Abstract
============
Introducción al Software Libre y su evolución, investigando la motivación de sus productores.

Descripción
===============
Intenta describir las razones por las cuales un modelo de negocio tan aparentemente 
extraño como regalar el producto es lógico, sustentable y permite la 
continuidad de la innovación.

Contenido
=========

- Inicios del movimiento
- Ideología
- Aplicaciones fundamentales
- Sustentabilidad ¿Cómo se financia?
- Beneficios más importantes para los desarrolladores

Recursos
=============
🎞 : `Video Flisol 2018 <https://youtu.be/WJ2mqfflHRE>`_

💾: `Diapositivas Porque hacemos Software Libre </PorqueSL.pdf>`_
