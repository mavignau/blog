.. title: Celular, un testigo posible y silencioso
.. date: 2022-02-07 23:22:34 UTC+03:00
.. tags: charlas historial refencias
.. category: stories
.. link:
.. description:
.. type: text

Abstract
============
Técnicas empleadas para el análisis forense en dispositivos de telefonía móvil.

Descripción
===============
Describiré las técnicas usadas para identificar al asesino de una mujer a partir del análisis y decodificación 
de los archivos extraídos de celulares secuestrados, recuperado como prueba luego de haber sido revendido varias veces. 
Se describe la elaboración de un software libre realizado en Python para la descodificación de los archivos almacenados por la app de OLX

Contenido
=========
En la charla se introducen conceptos de informática forense en dispositivos móviles, explicando las características de la toma de muestras, luego se da un repaso de conceptos referentes a la tecnología móvil celular que son necesarios para realizar la investigación forense. Por último, se muestra el uso de un software realizado a medida para investigar bases de datos de una app usando SQLite y realizando el informe con Jinja2.Describiré las técnicas usadas para identificar al asesino de una mujer a partir del análisis y decodificación de los archivos extraídos de celulares secuestrados, recuperado como prueba luego de haber sido revendido varias veces. Se describe la elaboración de un software libre realizado en Python para la descodificación de los archivos almacenados por la app de OLX


Recursos
=============
🎞 :`Video PyConAR 2018:2 <https://youtu.be/q2TKsFmlY4c>`_

🎞 : `Video PyDayNEA 2018 <https://youtu.be/OFbdHaTReyU>`_

💾: `Diapositivas Celular Testigo silencioso </Celular Testigo silencioso.pdf>`_

