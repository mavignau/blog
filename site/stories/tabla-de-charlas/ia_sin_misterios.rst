.. title: IA sin misterios: Entre datos, algoritmos y redes
.. date: 2022-02-07 23:22:34 UTC+03:00
.. tags: charlas ia instroduccion
.. category: stories
.. link:
.. description:
.. type: text

Abstract
============
Descubre los fundamentos de la IA, sus tipos, herramientas en Python, y su impacto ético, pasado y futuro.
¡Explora datos, redes y algoritmos sin misterios!

Descripción
===============
En esta charla dinámica desmitificaremos la inteligencia artificial (IA) y exploraremos cómo esta poderosa tecnología transforma el mundo. Diseñada para entusiastas de la tecnología, este recorrido rápido y entretenido te llevará desde los conceptos básicos hasta las aplicaciones más avanzadas de IA, con un enfoque práctico y accesible.

Comenzaremos con una introducción a la IA, explicando qué es y cómo funciona en términos sencillos, acompañado de ejemplos cotidianos que muestran su presencia en nuestra vida diaria. Luego, exploraremos los hitos más relevantes de su evolución, desde sus primeros pasos en los años 50 hasta los avances recientes en aprendizaje profundo y modelos generativos.

Descubriremos los conceptos fundamentales de IA, como redes neuronales, aprendizaje supervisado y no supervisado, y cómo se conectan para crear aplicaciones innovadoras. También analizaremos los tipos de IA, incluyendo IA débil y fuerte, GANs (Redes Generativas Antagónicas), LLMs (Modelos de Lenguaje Extensos), y sistemas como Retrieval-Augmented Generation (RAG), que combinan búsqueda y generación.

Para los programadores, revisaremos las bibliotecas más populares de Python, como TensorFlow, PyTorch y Scikit-learn, que facilitan el desarrollo de soluciones IA. También compartiremos tips para escribir prompts efectivos, esenciales para trabajar con modelos generativos como ChatGPT.

Finalmente, reflexionaremos sobre la ética y el futuro de la IA, abordando desafíos como el sesgo, la transparencia y la colaboración entre humanos y máquinas.

Prepárate para una experiencia enriquecedora que te equipará con conocimientos clave para adentrarte en el mundo de la inteligencia artificial sin complicaciones. ¡La IA nunca fue tan accesible!

Contenidos
==========

1. Introducción a la IA
   - ¿Qué es la IA?
   - Ejemplos cotidianos
   - Diferencia entre IA, ML y DL

2. Historia de la IA
   - Hitos clave: desde los años 50 hasta la actualidad
   - Avances recientes en modelos generativos

3. Conceptos fundamentales
   - Redes neuronales
   - Aprendizaje supervisado, no supervisado y reforzado

4. Tipos de IA
   - IA débil y fuerte
   - Redes Generativas Antagónicas (GAN)
   - Modelos de Lenguaje Extensos (LLM)
   - Retrieval-Augmented Generation (RAG)

5. Librerías de Python más usadas
   - TensorFlow
   - PyTorch
   - Scikit-learn
   - Otras herramientas útiles

6. Tips para prompts
   - Qué es un prompt
   - Cómo diseñar prompts efectivos
   - Ejemplos prácticos para modelos generativos

7. Ética y futuro de la IA
   - Sesgos y transparencia
   - Colaboración humano-máquina
   - Tendencias y retos emergentes

