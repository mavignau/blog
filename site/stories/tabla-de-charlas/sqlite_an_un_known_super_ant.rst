.. title: SQLite, an (un) known super ant
.. date: 2022-02-07 23:22:34 UTC+03:00
.. tags: charlas historial refencias
.. category: stories
.. link:
.. description:
.. type: text

Abstract
========
Juice up the world most implemented database from your python software.


Outline
=======
- Introduction: advantages, best and worst use cases.
- Why this little database is amazing and where it can be used at its best.
- SQLite & Python: best practices
- Use of context managers, row factories, executing many operations at once and the concept of PRAGMAs
- Transactions, isolation and concurrency
- Transactions management in SQLite, use of WAL, Rollback and no journaling modes. Isolation levels: exclusive, immediate and deferred
- Indexing
- Why and why not indexing. Partial index, expression index, FTS index.
- Special data types
- Date & Time data, personalized data types

Resources
=============

🎞 : `Video Europython 2021 <https://youtu.be/s7110IaMEOs>`_

✍ : `Presentation Europython 2021 <https://ep2021.europython.eu/talks/52iMBxp-sqlite-an-un-known-super-ant/>`_

💾: `Slides </SqliteAdvanced.pdf>`_

